#! /usr/bin/env ruby

def die(m); puts m; exit; end
def clear
  $stdout.write "\r\e[0K"
  $stdout.flush
end

program = ARGV.shift
args = []
while ARGV.first =~ /^\-/ do
  args << ARGV.shift
end
die "usage: fuzzy <name> <args>" if ARGV.empty?
begin
  query = ARGV.join ' '
  result = `which #{query}`.strip
  result = `find . -name "#{query}" 2> /dev/null`.strip if result.empty?
  args << result
  exec(program, *args) if fork.nil?
  Process.wait
rescue Interrupt => ex
end

clear
