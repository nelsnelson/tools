#! /usr/bin/env ruby

# creates an image sequence / GIF from an mp4
# requires ruby, ffmpeg, and imagemagick

puts "Install ffmpeg" or exit 1 if `which ffmpeg`.strip().empty?
puts "Install graphicsmagick" or exit 1 if `which gm`.strip().empty?

filename = ARGV[0]
start = ARGV[1]
duration = ARGV[2]

puts "usage: mp42gif.rb <file>" and exit unless filename and File.exists? filename

basename = File.basename filename, '.mp4'

puts "Converting #{filename}"

# make a folder for the images
# dir = "#{filename}_frames.d"
# require 'fileutils'
# FileUtils.mkdir_p dir

# generate a palette
# cmd="ffmpeg -y -loglevel panic -i \"#{filename}\" -vf fps=24:flags=lanczos,palettegen palette.png"
# puts "Executing: #{cmd}"
# `#{cmd}`

# convert the MP4 to 24 frames (4fps * 6s = 24)
# cmd="ffmpeg -y -loglevel panic -i \"#{filename}\" -y -f image2 -vf fps=fps=24 \"#{dir}/output%04d.jpg\""
# puts "Executing: #{cmd}"
# `#{cmd}`

# and also convert the MP4 to a GIF
# cmd="ffmpeg -y -loglevel panic -i \"#{filename}\" -pix_fmt rgb24 \"#{basename}.gif\""
if start and duration
cmd="ffmpeg -y -loglevel panic -i \"#{filename}\" -ss #{start} -t #{duration} -vf scale=720:-1,format=rgb24 \"#{basename}.gif\""
cmd="ffmpeg -y -loglevel panic -i \"#{filename}\" -ss #{start} -t #{duration} \"#{basename}.gif\""
cmd="ffmpeg -y -i \"#{filename}\" -ss #{start} -t #{duration} -vf fps=fps=24 \"#{basename}.gif\""
else
cmd="ffmpeg -y -loglevel panic -i \"#{filename}\" -vf scale=720:-1,format=rgb24 \"#{basename}.gif\""
cmd="ffmpeg -y -loglevel panic -i \"#{filename}\" \"#{basename}.gif\""
cmd="ffmpeg -y -i \"#{filename}\" -vf fps=fps=24 \"#{basename}.gif\""
end
puts "Executing: #{cmd}"
`#{cmd}`

# `ffmpeg -y -loglevel panic -i "#{filename}" -pix_fmt rgb24 -r 4 "#{basename}.gif"`
# `ffmpeg -y -loglevel panic -i "#{filename}" -i "palette.png" -pix_fmt rgb24 -r 4 "#{basename}.gif"`
# `ffmpeg -y -loglevel panic -i "#{filename}" -vf scale=320:-1,format=rgb8,format=rgb24 -r 4 "#{basename}.gif"`

# optimize the gif
# use graphicsmagick
#
#   mkdir -p ~/Downloads/opt
#   pushd ~/Downloads/opt
#   hg clone http://hg.code.sf.net/p/graphicsmagick/code graphicsmagick
#   pushd ~/Downloads/opt/graphicsmagick
#   configure
#   make
#   sudo make install
#   popd && popd
#
#`gm convert -layers Optimize "#{basename}.gif" "#{basename}.gif"`
# cmd="gm mogrify -delay 5 -loop 0 \"#{basename}.gif\""
# puts "Executing: #{cmd}"
# `#{cmd}`

puts "Done"
