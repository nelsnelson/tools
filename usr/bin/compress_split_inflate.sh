#! /usr/bin/env bash

# Compress a large amount of files
# Split
# Inflate

FILE="Pictures"
tar -cvj ./${FILE} | split -b 650m -d – "${FILE}."
#cat ${FILE}.* > ${FILE}
find . -name "${FILE}.*" -exec cat {} >> ${FILE} \;
tar -xvj ${FILE}

