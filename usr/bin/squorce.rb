#! /usr/bin/env ruby

require 'optparse'

module Gist
  def write(save_file, content, private_gist = false, description = nil)
    url = URI.parse(CREATE_URL)
    if PROXY_HOST
      proxy = Net::HTTP::Proxy(PROXY_HOST, PROXY_PORT)
      http  = proxy.new(url.host, url.port)
    else
      http = Net::HTTP.new(url.host, url.port)
    end
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_PEER
    http.ca_file = ca_cert
    req = Net::HTTP::Post.new(url.path)
    data = { 'files' => { save_file => content } }
    data.merge!({ 'description' => description }) unless description.nil?
    data.merge!({ 'public' => !private_gist })
    req.body = JSON.generate(data)
    user, password = auth()
    if user && password
      req.basic_auth(user, password)
    end
    response = http.start{|h| h.request(req) }
    case response
    when Net::HTTPCreated
      JSON.parse(response.body)['html_url']
    else
      puts "Creating gist failed: #{response.code} #{response.message}"
      exit(false)
    end
  end
end


script_name = File.basename __FILE__

options = {}
OptionParser.new do |opt|
  opt.banner = "Usage: #{script_name} [directory] [options]\n\n"
  opt.on('-o', '--output-document=file', 'Save consolidated file as... (default is the name of the specified or current directory)') { |v| options[:file] = v }
  opt.on('-s', '--file-type=type', 'Consolidated file-type') { |v| options[:file_type] = v }
  opt.on_tail("-?", "--help", "Show this message") do
    puts opt
    exit
  end
  opt.on_tail('-v', '--version', "Show version") do
    puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0' 
    exit
  end
  
  begin
    opt.parse!(ARGV)
  rescue OptionParser::InvalidOption => e
    puts opt
    exit
  end
end

options[:target] = ARGV.shift if not ARGV.empty?

target = options[:target] || Dir.getwd
save_file = options[:file] || target

content = ""

excluded_dirs  = [ ".git", ".svn", ".idea", "cobertura" ]
excluded_types = [ "png", "gif", "jpg", "tiff", "ico", "swf", "data", "class", "war", "jar", "log" ]

# Construct the find command

cmd  = "find '#{target}' "

cmd += "\\( "

for dir in excluded_dirs
  cmd += "! -regex '.*/\\#{dir}.*' "
end

for type in excluded_types
  cmd += "! -regex '.*\.#{type}' "
  cmd += "! -regex '.*\.#{type.upcase}' "
end

cmd += "\\)"

#puts "The find command is: #{cmd}"

# Gather the statistics on the collected files

puts `date`
puts ""
if File.directory?(target)
  puts "Scanning directory #{target}"
else
  puts "Scanning file #{target}"
end

results = `#{cmd}`.split("\n").sort  # Execute the find command

puts ""

counter=0

for f in results
  if File.exists? f and not File.directory? f
    counter += 1
    section = "#{counter}.) #{f}"
    content << "\n"
    content << "### #{section}"
    content << "\n"
    content << "### #{'-'*section.length}\n\n"
    content << IO.read(f)
    print "\r#{counter} source code files "; $stdout.flush()
  end
end

puts ""

begin require 'syntax/convertors/html' rescue puts "# gem install syntax" end
convertor = Syntax::Convertors::HTML.for_syntax 'ruby'

puts convertor.convert(content)

