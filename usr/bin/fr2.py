#! /usr/bin/env python

import re
import os
import sys
import subprocess
import urllib
import httplib
import tempfile
from HTMLParser import HTMLParser
from urllib2 import urlopen

class MWParser(HTMLParser):
    def __init__(self,url):
        HTMLParser.__init__(self)
        req = urlopen(url)
        self.feed(req.read())

    def handle_starttag(self, tag, attrs):
        if tag == 'a' and attrs:
            #print "Found link => %s" % attrs[0][1]
            return 

def which(name):
    return subprocess.Popen(['which',name],stdout=subprocess.PIPE).communicate()[0].strip()

class Vocabulary:
    def __init__(self, words):
        self.words = words
        self.ogg_urls = {} 
        for word in words:
            word = word.replace('\"', '')

            print 'Getting definition for \'%s\'' % word

            try:
                wiktionary_url = 'http://fr.wiktionary.org/wiki/%s' % word

                host = 'fr.wiktionary.org'
                conn = httplib.HTTPConnection(host, 80)

                url = '/wiki/%s' % word
                conn.request('GET', url)

                response = conn.getresponse()

                content = response.read()
                conn.close()

                results = re.findall('http[\S]*ogg', content)
                for match in results:
                    self.ogg_urls[word] = match

                MWParser('http://%s%s' % (host, url))
            except Exception, e:
                print e

    def empty(self):
        return len(self.ogg_urls.values()) == 0

    def speak_all(self):
        player = '/usr/bin/afplay'
        a = which('afplay') 
        b = which('aplay')
        if a:
            player = a
        elif b:
            player = b
        for word in self.words:
            url = self.ogg_urls[word]
            d = urllib.urlopen(url)
            f = open(url.split('/')[-1], 'w')
            f.write(d.read())
            d.close()
            f.close()
            os.system(player + ' ' + f.name)
            os.remove(f.name)

sys.argv.pop(0)
words = sys.argv
voc = Vocabulary(words)

if voc.empty():
    print "Couldn't find any words"
    sys.exit()

voc.speak_all()


