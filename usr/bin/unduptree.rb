#! /usr/bin/env ruby

target = ARGV.shift

unless target
  puts "Please specify a target directory."
  exit
end

references = []
duplicates = []

files = Dir['*']

for file in files
  next unless File.exists?(file)
  size = File.size(file)
  references << [file,size]
end

puts "Scanning for duplicates in #{target}..."

for file,size in references
  match = `find #{target} -name "#{file}"`.strip
  next if match.empty?
  size2 = File.size(match)
  duplicates << file if size == size2
end

if duplicates.empty?
  puts "No duplicates found."
  exit
end

puts "Found #{duplicates.length} duplicates:"

for duplicate in duplicates
  puts "#{duplicate}"
end

$stdout.write "Delete duplicates? (y/N)>"
answer = gets

exit unless answer =~ /(y|yes)/

puts "Deleting duplicates..."

for file in duplicates
  if File.directory?(file) and file != '/'
    `rm -rf "#{file}"`
  else
    File.delete(file)
  end
end
