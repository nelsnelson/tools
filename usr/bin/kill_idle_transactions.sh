#! /usr/bin/env bash

count=0
for pid in $(pgrep -f "core_dev.*idle in transaction")
do
    count=$(($count + 1))
    kill $pid
done
echo "Killed $count idle processes"

