#! /usr/bin/env python

import re
import os
import sys
import subprocess
import urllib
import httplib
import tempfile
from HTMLParser import HTMLParser
from urllib2 import urlopen

class MWParser(HTMLParser):
    def __init__(self,url):
        HTMLParser.__init__(self)
        req = urlopen(url)
        self.feed(req.read())

    def handle_starttag(self, tag, attrs):
        if tag == 'a' and attrs:
#            for i in range(len(attrs[0])):
#                print "Found link => %s" % attrs[0][i]
            return 

def which(name):
    return subprocess.Popen(['which',name],stdout=subprocess.PIPE).communicate()[0].strip()

class Vocabulary:
    def __init__(self, words):
        self.words = words
        self.ogg_urls = {} 
        for word in self.words:
            word = word.replace('\"', '')

            print 'Getting definition for \'%s\'' % word 

            try:
                url = 'http://www.merriam-webster.com/dictionary/%s' % word

                self.host = 'www.merriam-webster.com'
                conn = httplib.HTTPConnection(self.host, 80)

                file = word[:6]
                n = (7 - len(file))
                for i in range(0,n):
                    file = file + '0'
                file = file + '1'

                url = '/dictionary/%s' % word
                conn.request('GET', url)#, body=urllib.urlencode(params), headers=headers)

                response = conn.getresponse()

                content = response.read()
                conn.close()

                results = re.findall('[^\']*wav[^\']*', content)
                for match in results:
                    print match
                    self.ogg_urls[word] = match

                url = self.ogg_urls[word]
                
            except Exception, e:
                print e

    def empty(self):
        return len(self.ogg_urls.values()) == 0

    def speak_all(self):
        player = '/usr/bin/afplay'
        a = which('afplay') 
        b = which('aplay')
        if a:
            player = a
        elif b:
            player = b
        for word in self.words:
            url = 'http://%s%s' % (self.host, self.ogg_urls[word])
            print url
            d = urllib.urlopen(url)
            name = '%s.wav' % url.split('=')[-1]
            f = open(name, 'w')
            f.write(d.read())
            d.close()
            f.close()
            os.system(player + ' ' + f.name)
            os.remove(f.name)

sys.argv.pop(0)
words = sys.argv
voc = Vocabulary(words)

if voc.empty():
    print "Couldn't find any words"
    sys.exit()

voc.speak_all()


