#! /usr/bin/env ruby

class Program
  def initialize
    run
  rescue Interrupt, SystemExit
  rescue Exception => ex
    puts ex
    puts ex.backtrace
  end

  def options
    @options ||= parse_args
  end
end

class Main < Program
  def initialize
    unless options[:dir]
      puts "You must specify a directory containing a subversion project that you wish to copy to a new branch."
      exit
    end
    unless options[:issue]
      puts "You must specify an issue tracking number."
      exit
    end
    super
  ensure
    clear
  end

  def run
    dir = options[:dir]
    issue = options[:issue]
    branch_dir = options[:branch_dir] || dir.gsub(/\-trunk/, '') + issue.gsub(/[\D]/, '')
    commit_title = options[:title] || 'Initial Branch'
    commit_title = commit_title.split if commit_title.respond_to? :split
    branch_title = commit_title.join '-'
    orig_url = `svn info #{dir}`.strip[/^URL: (.*)$/][/http.*/]
    orig_url_parts = orig_url.split(/(\/branches\/|\/trunk\/?)/)
    base_url = orig_url_parts.first
    date = Time.now.strftime('%Y-%m-%d')
    branch_url = "#{base_url}/branches/#{issue}-#{branch_title}-#{date}"
    orig = orig_url =~ /\/trunk\/?/ ? "trunk" : orig_url_parts.last
    message = options[:message] || "Branch from #{orig} for development on this issue.\n"
    message = "#{issue} #{commit_title.join ' '}\n\n#{message}"
    suffix = options[:suffix]

    unless branch_dir =~ /([\d]{4}-[\d]{2}-[\d]{2})/
      branch_dir = "#{branch_dir}-#{date}"
    end

    if suffix
      branch_url = "#{branch_url}-#{suffix}"
    end

    unless options[:skip_verification]
    puts "Original is:    #{orig_url}"
    puts "Branch will be: #{branch_url}"
    exit unless yesorno
    puts "Okay."
    puts "Branch message is:"
    puts "#{message}\n"
    exit unless yesorno
    puts "Original directory is:    #{dir}"
    puts "Branch directory will be: #{branch_dir}" 
    exit unless yesorno
    end

    progress("Reverting...") { `svn revert #{dir}` } 
    progress("Updating...") { `svn up #{dir}` } 
    progress("Branching...") { `svn copy #{orig_url} #{branch_url} -m "#{message}" --quiet` }
    progress("Checking out #{branch_url} to #{branch_dir}") { `svn checkout #{branch_url} #{branch_dir} --quiet` }
    Dir.chdir branch_dir
    puts "\e[34m#{Dir.pwd}\e[0m:"
    puts `ls`
  end
end

def parse_args
  require 'optparse'

  script_name = File.basename __FILE__

  options = {}
  OptionParser.new do |opt|
    opt.banner = "Usage: #{script_name} <dir> <issue> [title] [options...]\n\n"
    opt.on('-d', '--dir=dir', 'Directory containing original branch') { |v| options[:dir] = v }
    opt.on('-o', '--output-directory=dir', 'Directory in which svn checkout will put the branch copy') { |v| options[:branch_dir] = v }
    opt.on('-t', '--title=title', 'Title information') { |v| options[:title] = v }
    opt.on('-m', '--message=message', 'Commit message') { |v| options[:message] = v }
    opt.on('-n', '--name=name', 'Name of the branch') { |v| options[:name] = v }
    opt.on('-i', '--issue=issue', 'Issue tracking number or tag') { |v| options[:issue] = v }
    opt.on('-s', '--suffix=suffix', 'Unique suffix for url') { |v| options[:suffix] = v }
    opt.on('-q', '--skip-verification', 'Skip verification') { |v| options[:skip_verification] = v }
    opt.on_tail("-?", "--help", "Show this message") do
      puts opt
      exit
    end
    opt.on_tail('-v', '--version', "Show version") do
      puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0'
      exit
    end

    begin
      opt.parse!(ARGV)
    rescue OptionParser::InvalidOption => e
      puts opt
      exit
    end
    @options = opt
  end

  options[:dir] = ARGV.shift if not ARGV.empty?
  options[:issue] = ARGV.shift if not ARGV.empty?
  while not ARGV.empty?
    options[:title] ||= []
    options[:title] << ARGV.shift
  end
  options
end

def usage
  puts OPTIONS.help
  exit
end

def yesorno(question="Continue?")
  ask(question, 'Y/n') =~ /y/i
end

def ask(question, options='y/N', default=options.scan(/.*([A-Z]).*/).flatten.first)
  prompt "#{question} (#{options})> ", {
    :constraints => options.split(/\//),
    :default => default
  }
end

def prompt(prompt, options={})
  constraints = options[:constraints]
  while true
    put prompt
    response = gets.chomp
    return options[:default] if response.empty?
    return response unless constraints
    if response =~ /(#{constraints.join('|')})/i
      return response
    else
      puts "Please respond with #{constraints[0..-2].join(', ')} or #{constraints[-1]}"
    end
  end
end

def progress(message, &block)
  put message
  t = Thread.new do
    a = [ ".", "..", "..."]
    i=0
    while true
      sleep 1
      status " %s%s" % [ message, a[i] ]
      i+=1
      i%=3
    end
  end
  yield block
  t.kill
end

def put(s)
  $stdout.write s
  $stdout.flush
end

def clear
  put "\r\e[0K"
end

def status(s)
  clear
  put "%s\r" % s
end

Main.new if __FILE__ == $0

