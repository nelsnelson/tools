#! /usr/bin/env ruby

MustBeRoot = true

class Program
  def initialize
    run
  rescue Interrupt, SystemExit
  rescue Exception => ex
    puts ex
  end

  def options
    @options ||= parse_args
  end
end

class Main < Program
  def initialize
    options[:host]
    super
  end

  def run
    root_prompt unless root?
    options[:keytool] ||= `which keytool`.strip
    raise "Cannot find keytool binary" unless File.exists? options[:keytool]
    options[:keystore] ||= if ENV['JAVA_HOME']
      `find #{ENV['JAVA_HOME']} -name cacerts`.strip.split.first
    elsif File.exists? '/usr/libexec/java_home'
      `find $(/usr/libexec/java_home) -name cacerts`.strip.split.first
    else
      '/System/Library/Java/Support/CoreDeploy.bundle/Contents/Home/lib/security/cacerts'
    end
    unless File.exists? options[:keystore]
      raise "Could not find the keystore file to list: please specify using -k or --keystore"
    end

    if options[:cleanup]
      cleanup_certs
    else
      puts list_certs
    end
  end
end

def parse_args
  require 'optparse'

  script_name = File.basename __FILE__

  options = {}
  OptionParser.new do |opt|
    opt.banner = "Usage: #{script_name} [keystore file] [options...]\n\n"
    opt.on('-c', '--cleanup', 'Cleanup the given keystore file.') { |v| options[:cleanup] = true }
    opt.on('-b', '--keytool=keytool', 'The keytool binary.') { |v| options[:keytool] = v }
    opt.on('-d', '--debug', 'Do you want to see shell commands printed out during execution?') { |v| options[:debug] = true }
    opt.on_tail("-?", "--help", "Show this message.") do
      puts opt
      exit
    end
    opt.on_tail('-v', '--version', "Show version.") do
      puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0'
      exit
    end

    begin
      opt.parse!(ARGV)
    rescue OptionParser::InvalidOption => e
      puts opt
      exit
    end

    options[:keystore] ||= ARGV.shift unless ARGV.empty?
  end

  options
end

def root?
  Process.uid == 0
end

def root_prompt
  `sudo -p \"Root password: \" echo`
end

def list_certs
  cmd = "#{options[:keytool]} -list -keystore #{options[:keystore]} -deststorepass changeit"
  puts "  $ #{cmd}" if options[:debug]
  `#{cmd}`.strip
end

def cleanup_certs
  list_certs.scan(/([\w\d\.\-]+\.crt)/) do |v|
    delete_alias $1
  end
end

def delete_alias(host)
  puts "Deleting old alias for #{host} from existing keystore file: #{options[:keystore]}"
  cmd = "sudo #{options[:keytool]} -delete -trustcacerts -alias #{host} -keystore #{options[:keystore]} -deststorepass changeit -noprompt"
  puts "  $ #{cmd}" if options[:debug]
  `#{cmd}`
end

Main.new if __FILE__ == $0
