#! /usr/bin/env ruby

require 'uri'

class Program
  def initialize
    run
  rescue Interrupt, SystemExit 
    clear
  end

  def options
    @options ||= parse_args
  end
end

class Main < Program
  def initialize
    unless options[:target] and (File.exists? options[:target] or (!(options[:target] =~ URI::regexp).nil?))
      puts "You must specify a directory containing or url to a subversion project."
      exit
    end
    super
  end

  def run
    target = options[:target]
    from_revision = `svn log --stop-on-copy #{target}`.scan(/^r(\d+)\s\|/).last
    if options[:brief] then puts "#{from_revision}" else puts "Base branch revision is #{from_revision}" end
  end
end

def parse_args
  require 'optparse'

  script_name = File.basename __FILE__

  options = {}
  OptionParser.new do |opt|
    opt.banner = "Usage: #{script_name} <dir> [title] [options...]\n\n"
    opt.on('-b', '--brief', 'Brief.  The opposite of verbose.') { |v| options[:brief] = v }
    opt.on('-d', '--dir=dir', 'Directory containing the target project') { |v| options[:target] = v }
    opt.on('-u', '--url=url', 'URL for the target project repository') { |v| options[:target] = v }
    opt.on_tail("-?", "--help", "Show this message") do
      puts opt
      exit
    end
    opt.on_tail('-v', '--version', "Show version") do
      puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0'
      exit
    end

    begin
      opt.parse!(ARGV)
    rescue OptionParser::InvalidOption => e
      puts opt
      exit
    end
  end

  options[:target] = ARGV.shift if not ARGV.empty?
  options[:target] ||= Dir.getwd
  options
end

def yesorno
  response = prompt "Continue? (Y/n)> ", { :constraints => [ 'y', 'n' ] }
  exit if response =~ /n/
end

def prompt(prompt, options={})
  constraints = options[:constraints]
  while true
    put prompt
    response = gets.chomp
    return response unless constraints
    if response =~ /(#{constraints.join('|')})/i
      return response
    else
      puts "Please respond with #{constraints[0..-2].join(', ')} or #{constraints[-1]}"
    end
  end
end

def progress(message, debug=false, &block)
  put message
  t = Thread.new do
    a = [ ".", "..", "..."]
    i=0
    while true
      sleep 1
      status " %s%s" % [ message, a[i] ]
      i+=1
      i%=3
    end
  end
  output = yield block
  put output if debug
  t.kill
end

def put(s)
  $stdout.write s
  $stdout.flush
end

def clear
  put "\r\e[0K"
end

def status(s)
  clear
  put "%s\r" % s
end

Main.new if __FILE__ == $0

