#! /usr/bin/env ruby

require 'optparse'

script_name = File.basename __FILE__

options = {}
OptionParser.new do |opt|
  opt.banner = "Usage: #{script_name} [directory] [options]\n\n"
  opt.on('-s', '--file-type=type', 'Consolidated file-type') { |v| options[:sort_by] = v }
  opt.on_tail("-?", "--help", "Show this message") do
    puts opt
    exit
  end
  opt.on_tail('-v', '--version', "Show version") do
    puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0' 
    exit
  end
  
  begin
    opt.parse!(ARGV)
  rescue OptionParser::InvalidOption => e
    puts opt
    exit
  end
end

options[:target] = ARGV.shift if not ARGV.empty?

target = options[:target] || Dir.getwd

Stat = Struct.new(:files, :lines, :bytes)
statistics = {}

excluded_dirs  = [ ".git", ".svn", ".idea", "cobertura" ]
excluded_types = [ "png", "gif", "jpg", "tiff", "ico", "swf", "data", "class", "war", "jar", "log" ]

# Construct the find command

cmd  = "find '#{target}' "

cmd += "\\( "

for dir in excluded_dirs
  cmd += "! -regex '.*/\\#{dir}.*' "
end

for type in excluded_types
  cmd += "! -regex '.*\.#{type}' "
  cmd += "! -regex '.*\.#{type.upcase}' "
end

cmd += "\\)"

#puts "The find command is: #{cmd}"

# Gather the statistics on the collected files

puts `date`
puts ""
if File.directory?(target)
  puts "Scanning directory #{target}"
else
  puts "Scanning file #{target}"
end

results = `#{cmd}`.split("\n")  # Execute the find command
 
puts ""
 
counter=0

for f in results
  if File.exists? f and not File.directory? f
    extension = File.extname(f)
    stat = statistics[extension] || Stat.new(0, 0, 0)
    stat.files += 1
    stat.lines += File.new(f).readlines.length
    stat.bytes += File.size(f)
    statistics[extension] = stat
    counter += 1
    print "\r#{counter} source code files "; $stdout.flush()
  end
end

total_files = statistics.values.inject(0) { |x, n| x + Integer(n.files) rescue 0 }
total_lines = statistics.values.inject(0) { |x, n| x + Integer(n.lines) rescue 0 }
total_bytes = statistics.values.inject(0) { |x, n| x + Integer(n.bytes) rescue 0 }

case options[:sort_by]
  when 'type'
    statistics = statistics.sort
  when 'files'
    statistics = statistics.sort { |a,b| b[1].files <=> a[1].files }
  when 'bytes'
    statistics = statistics.sort { |a,b| b[1].bytes <=> a[1].bytes }
  else
    statistics = statistics.sort { |a,b| b[1].lines <=> a[1].lines }
end
 
# Display the report

puts ""
puts ""
  puts "  %-15s  %15s  %15s  %15s" % [ "extension", "number of files", "lines of code", "bytes" ]
  puts "  %-15s  %15s  %15s  %15s" % [ "---------", "---------------", "-------------", "-----" ]
for extension, stat in statistics
  puts "  \*%-14s  %15d  %15d  %15s" % [ extension, stat.files, stat.lines, stat.bytes ]
end 

puts ""
puts "Totals: %26d  %15d  %15s" % [ total_files, total_lines, total_bytes ]
puts ""

