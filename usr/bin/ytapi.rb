#! /usr/bin/env ruby

$: << File.expand_path(File.dirname(__FILE__))

require 'hash_ext'
require 'nokogiri'
require 'open-uri'
require 'openssl'
require 'yaml'

url = "https://gdata.youtube.com/feeds/api/videos"
query = ARGV.join('+')
params = URI::encode "q=#{query}&max-results=1&v=2" #&orderby=published&start-index=0&max-results=1&prettyprint=true&v=2"
options = {
  :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE
}

puts "Connecting to #{url}?#{params}"
begin
  result = open("#{url}?#{params}", options).read
  xml = Nokogiri::XML(result)
  result = Hash.from_xml xml.to_xml
  puts result.feed.entry.group.videoid
rescue OpenURI::HTTPError => ex
  puts "Error: #{ex.message}"
  puts ex.backtrace
end

