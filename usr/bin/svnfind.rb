#! /usr/bin/env ruby

require 'rubygems'
require 'nokogiri'

@user     = ARGV.shift
@url      = ARGV.shift || 'https://source.myrs.rackspace.com/repos/legacy/myrackspace_v6/branches/'
@field    = ARGV.shift || 'author'
@filter   = ARGV.shift || 'name'
@branches = Nokogiri::XML(`svn ls --xml #{@url}`).xpath("//lists//list//entry")

puts "#{@branches.length} branches in #{@url}"

for entry in @branches do
end

@results = @branches.select { |entry| 
  entry.css('author').first.content =~ /^#{@user}$/
}.collect { |entry| 
    "#{@url}#{entry.css('name').first.content}"
}

puts "#{@results.length} branches were found which match your criteria"

puts @results