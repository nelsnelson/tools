#! /usr/bin/env ruby

# Currently single use script application for Groovy language script source
# code files only.

# Will "gut" out the contents and implementation of a Groovy/Java class or
# interface.

d = ARGV.shift || '.'
f = ARGV.shift || '*.groovy'

`find . -path '*#{d}*' -name '#{f}'`.strip.split.each { |file|
    contents = IO.readlines(file).join('').split("\r?\n").join('======')

    next if contents.match(/implements/) or contents.match(/static final/)

    if contents.match(/([^\{]+.*(public ?)?class [^\{]+)\{/)
      preamble = $1.split('======').join("\n") if $1
      out = File.open(file, 'w')
      out.puts "#{preamble}{}"
    end
}
