#!/usr/bin/env ruby
#
# Converts all MP4 files in the current directory to MP3s.
#

require 'docopt'

Mplayer = `which mplayer`
Lame = `which lame`
Program = File.basename($0)
Usage = <<USAGE
#{Program}: Converts all given MP4 files or those in the current directory to MP3s.

Usage:
  #{Program} [<mp4-files>...] [--wet-run]
  #{Program} -h | --help
  #{Program} --version

 Options:
   --wet-run        Perform the conversions.
   -h, --help       Show this message.
   --version        Print the version.
USAGE
def options
  Docopt::docopt(Usage, version: '1.0')
rescue Docopt::Exit => ex
  puts ex.message
  exit
end
Options = options

def convert(f)
  filename = File.basename(f, '.mp4')
  wav = "/tmp/#{filename}.wav"
  command = "#{Mplayer} \"#{f}\" -cache 4096 -vc null -vo null -ao pcm:file=\"#{wav}\""
  puts command unless Options['--wet-run']
  # `#{command}`
  mp3 = "#{filename}.mp3"
  wav.gsub!(/([\'\s\(\)])/) { "\\#{$1}" }
  mp3.gsub!(/([\'\s\(\)])/) { "\\#{$1}" }
  command = "#{Lame} -S -h -V 0 -q 2 #{wav} \"#{mp3}\""
  puts command unless Options['--wet-run']
  # `#{command}`
  # File.delete wav if File.exists? wav
end

def mp42mp3
  die "MPlayer is not installed" unless Mplayer
  die "Lame is not installed" unless Lame

  if not Options.include? '<mp4-files>' or Options['<mp4-files>'].empty?
    files = Dir.glob("*.mp4")
  else
    files = Options['<mp4-files>']
  end

  files.each do |f|
    convert f
  end
end

def die(message)
  puts message
  exit
end

def main
  mp42mp3
rescue Interrupt
  puts "\rUser interrupted"
rescue Exception => error
  puts error.message
  puts error.backtrace
end

main if __FILE__ == $0
