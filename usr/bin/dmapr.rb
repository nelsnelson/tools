#! /usr/bin/env ruby

require 'rubygems'
require 'dm-core'
require 'optparse'

script_name = File.basename __FILE__

options = {}
OptionParser.new do |opt|
  opt.banner = "Usage: #{script_name} <database> [options]"
  opt.on('-h', '--host=host', 'Specify the database host server address.') { |v| options[:host] = v }
  opt.on('-f', '--dbfile=dbfile', 'Specify a database file to use.') { |v| options[:dbfile] = v }
  opt.on('-d', '--database=database', 'Specify a database to use.') { |v| options[:database] = v }
  opt.on('-h', '--host=address', 'Specify the database host server to use.') { |v| options[:host] = v }
  opt.on('-p', '--port=port', 'Specify the the port on which to attempt connection.') { |v| options[:port] = v }
  opt.on('-u', '--username=username', 'Specify the user role to use.') { |v| options[:username] = v }
  opt.on('-W', '--password=password', 'Specify the user password to use.') { |v| options[:password] = v }
  opt.on('-a', '--adapter=adapter', 'Specify a database adapter to use.') { |v| options[:adapter] = v }
  opt.on('-c', '--config=path', 'Specify a config file to use (default is ~/.active.yml).') { |v| options[:config] = v }
  opt.on("--debugger", 'Enable ruby-debugging for the console.') { |v| options[:debugger] = v }
  opt.on("--logger", 'Enable SQL logging for the console.') { |v| options[:logging] = v }
  opt.on_tail("-?", "--help", "Show this message") do
    puts opt
    exit
  end
  opt.on_tail('-v', '--version', "Show version") do
    puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0'
    exit
  end

  opt.parse!(ARGV)
end

unless ARGV.empty? or options[:database]
  options[:database] = ARGV.shift
end

if options[:logging]
DataMapper::Logger.new(STDOUT, :debug)
end

default = 'postgres://core_write:@d-db1.core.rackspace.com:5432/core_dev'
DataMapper.setup(
    :default, options[:database_connection_url] || ARGV.shift || default
)

class IpInfo
  include DataMapper::Resource
  storage_names[:default]='IPSP_cache_IPAssignment'
#  def self.storage_name(repository)
#    "IPSP_cache_IPAssignment"
#  end
end

ARGV.clear
require 'irb'
IRB.start

