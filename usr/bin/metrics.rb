#! /usr/bin/env ruby

require 'optparse'
require 'thread'

script_name = File.basename __FILE__

@excluded_dirs  = [ '.git', '.svn', '.idea', 'cobertura', '.venv', 'env', 'venv', 'cover' ]
@excluded_types = [ 'class', 'data', 'gif', 'ico', 'jar', 'jpg', 'log', 'png', 'pyc', 'swf', 'tiff', 'war' ]
@valid_sorts    = [ 'bytes', 'files', 'lines', 'type' ]

options = {}
OptionParser.new do |opt|
  opt.banner = "Usage: #{script_name} [directory,...] [options]\n\n"
  opt.on('-d', '--depth=N', 'Depth to which tree is recursed to gather statistics.') { |v| options[:depth] = v }
  opt.on('-x', '--exclude=paths', 'Exclude list of paths delimited by commas from counts.') { |v| options[:exclude] = v }
  opt.on('-s', '--sort-by=field', 'How the results should be sorted.', 'Where field is one of: lines (default), files, type, bytes') { |v|
    raise OptionParser::InvalidOption.new('Invalid sort field') unless @valid_sorts.include? v
    options[:sort_by] = v
  }
  opt.on_tail("-?", "--help", "Show this message") do
    puts opt
    exit
  end
  opt.on_tail('-v', '--version', "Show version") do
    puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0' 
    exit
  end
  begin
    opt.parse!(ARGV)
  rescue OptionParser::InvalidOption => e
    puts "#{e}\n\n#{opt}"
    exit
  end
end

options[:target] ||= []
while not ARGV.empty? do
  options[:target] << ARGV.shift
end

targets = options[:target]
targets = [ Dir.getwd ] if targets.empty? 
@excluded_dirs.concat options[:exclude].split(',') if options[:exclude]
@depth = options[:depth]

Stat = Struct.new(:files, :lines, :bytes)
@statistics = {}

puts `date`
puts ""

def find(target)
  # Construct the find command

  cmd  = "find '#{target}' -type f "

  cmd += "-depth #{@depth} " if @depth

  cmd += "\\( "

  for dir in @excluded_dirs
    cmd += "! -regex '.*/\\#{dir}.*' "
  end

  for type in @excluded_types
    cmd += "! -regex '.*\.#{type}' "
    cmd += "! -regex '.*\.#{type.upcase}' "
  end

  cmd += "\\)"
end

# Gather the statistics on the collected files
results = []

for target in targets
  if File.directory?(target)
    puts "Scanning directory #{target}"
  else
    puts "Scanning file #{target}"
  end
  cmd = find target
  #puts "The find command is: #{cmd}"
  results.concat `#{cmd}`.split("\n")  # Execute the find command
end

puts ""

@counter=0

def count(f)
  extension = File.extname(f)
  stat = @statistics[extension] || Stat.new(0, 0, 0)
  stat.files += 1
  stat.lines += `wc -l "#{f}"`.strip.to_i
  stat.bytes += File.size(f)
  @statistics[extension] = stat
  @counter += 1
end

def count_files(files)
  queue     = Queue.new
  semaphore = Mutex.new
  files.each { |f| queue << f }
  4.times.map {
    Thread.new do
      while !queue.empty?
        f = queue.pop
        count(f)
        semaphore.synchronize {
          yield @counter 
        }
      end
    end
  }.each {|thread| thread.join }
end
#count_files(results) do |counter|
#  print "\r#{counter} source code files "; $stdout.flush()
#end

for f in results
  if File.exists? f and not File.directory? f
    extension = File.extname(f)
    stat = @statistics[extension] || Stat.new(0, 0, 0)
    stat.files += 1
    stat.lines += `wc -l "#{f}"`.strip.to_i
    stat.bytes += File.size(f)
    @statistics[extension] = stat
    @counter += 1
    print "\r#{@counter} source code files "; $stdout.flush()
  end
end

total_files = @statistics.values.inject(0) { |x, n| x + Integer(n.files) rescue 0 }
total_lines = @statistics.values.inject(0) { |x, n| x + Integer(n.lines) rescue 0 }
total_bytes = @statistics.values.inject(0) { |x, n| x + Integer(n.bytes) rescue 0 }

field = (options[:sort_by] || 'lines').to_sym
puts "sorted by #{field}"

@statistics = if field and @statistics.all? { |k,v| v.members.include? field } then @statistics.sort { |a,b| b[1].send(field) <=> a[1].send(field) } else @statistics.sort end

# Display the report

puts ""
  puts "  %-15s  %15s  %15s  %15s"   % [ "extension", "number of files", "lines of code", "bytes"    ]
  puts "  %-15s  %15s  %15s  %15s"   % [ "---------", "---------------", "-------------", "-----"    ]
for extension, stat in @statistics
  puts "  \*%-14s  %15d  %15d  %15s" % [ extension,   stat.files,        stat.lines,      stat.bytes ]
end 

puts ""
puts "Totals: %26d  %15d  %15s" % [ total_files, total_lines, total_bytes ]
puts "Lines per file: %18.2f" % [ (total_lines / total_files.to_f) ] if total_files > 0
puts "Code density: %20.2f" % [ (total_bytes / total_lines.to_f) ] if total_lines > 0
puts ""

