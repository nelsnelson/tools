#! /usr/bin/env jruby

$: << File.expand_path(File.dirname(__FILE__) + "/")

require 'java'
require 'find'
require 'fast-md5.jar'

java_import com.twmacinta.util.MD5

def put(s)
  $stdout.write s; $stdout.flush
end

dir1 = ARGV.shift
dir2 = ARGV.shift

exit unless dir1 and dir2

def finddups(dir1, dir2)
  dir1path = File.expand_path(dir1)
  dir2path = File.expand_path(dir2)
  dir1_files   = Hash.new
  dir1_digests = Hash.new
  dir2_files   = Hash.new
  dir2_digests = Hash.new

  puts "Calculating MD5 digests for files in first directory (#{dir1path})..."
  Find.find(dir1path) { |f|
    next if File.directory? f
    path   = File.expand_path(f)
    file   = File.basename(path)
    digest = MD5.asHex(MD5.getHash(java.io.File.new(path)))
    dir1_digests[digest] = file
    dir1_files[file] ||= Array.new
    dir1_files[file] << File.dirname(path)
  }

  puts "Calculating MD5 digests for files in second directory (#{dir2path})..."
  Find.find(dir2path) { |f|
    next if File.directory? f
    path   = File.expand_path(f)
    file   = File.basename(path)
    digest = MD5.asHex(MD5.getHash(java.io.File.new(path)))
    dir2_digests[digest] = file
    dir2_files[file] ||= Array.new
    dir2_files[file] << File.dirname(path)
  }

  return dir1_digests, dir2_digests, (dir1_digests.keys & dir2_digests.keys)
end

dir1_digests, dir2_digests, duplicates = finddups(dir1, dir2)
puts duplicates.map { |digest| dir1_digests[digest] }.join(" ")
