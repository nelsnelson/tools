#! /usr/bin/env ruby

require 'net/ssh'
require 'timeout'

ip = ARGV.shift
if not ip then puts "You must enter an IP address."; exit end
username = ARGV.shift || 'nnelson'
password = "Ithaca^12"
subnet = ip[/\d+\.\d+\.\d+\./]
puts subnet

begin
(150..156).each do |i|
  target = "#{subnet}#{i}"
  begin
    Timeout::timeout(3) do
      begin
        Net::SSH.start(target, username, :password => password) do |ssh|
          output = ssh.exec!("hostname")
          puts "#{username}@#{target}: Connected! (#{output})"
        end
      rescue Errno::ECONNREFUSED
        puts "#{username}@#{target}: Connection refused"
      rescue Errno::EHOSTUNREACH
        puts "#{username}@#{target}: No Route To Host"
      rescue Net::SSH::AuthenticationFailed
        puts "#{username}@#{target}: Auth failure"
      rescue Net::SSH::HostKeyMismatch
        puts "#{username}@#{target}: Host Key Mismatch"
      rescue Net::SSH::Disconnect
        puts "#{username}@#{target}: Disconnected"
      rescue SocketError
        puts "Bad address: #{target}"
      end
    end
  rescue Timeout::Error
    puts "#{username}@#{target}: Connection timed out"
  end
end
rescue Interrupt
  print "\r"; $stdout.flush
  puts "Stopped"
end
