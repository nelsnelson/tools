#! /usr/bin/env bash

svn=`which svn`
[[ -z "${svn}" ]] && { echo "Please make your path include svn"; exit; }
[[ -n "${1}" ]] && target="${1}" || target=`pwd`
echo "Searching ${target} for unresolved sources"

for f in ${target}/**/presentation/application.properties; do
    quartz_is_installed=`grep "plugins.quartz=" ${f}`
    presentation=`dirname ${f}`
    project=`dirname ${presentation}`
    status=`${svn} status $f 2>&1`
    if [ -z "${status}" ]; then
        echo "${project}: clean"
    elif [ -n "${status}" ]; then
        echo "${project}: ${status}"
        continue
        result=`echo "${status}" | grep "application.properties"`
        if [ -n "${result}" ]; then
            result=`${svn} diff ${dir}/presentation/application.properties | grep "\-plugins.quartz=0.4.2"`
            if [ -n "${result}" ]; then
                echo "${dir}/presentation/application.properties"
            fi
        fi
        #$svn up --accept=postpone
    fi
done
