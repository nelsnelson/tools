#!/bin/bash

###########################################
# Update FileVault password
#
# This short script will assist user into
# updating FileVault password thanks to
# High Sierra FileVault bug.
#
# To do:
# - Make it interactive or something
# - Error trapping
#
# Written by:
# Emilio Lechler, Jr., ACMT
# Senior Service Desk Technician
# Rackspace US, Inc.
#
# Tested by:
# Dorian Cordero, ACMT
# #mac-users
###########################################

# Check if running as root
if [[ $(/usr/bin/id -u) != 0 ]]; then
   echo "Please run this script as root."
   exit 1
fi

# Grab logged in user
SSO=$(/usr/bin/python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; import sys; username = (SCDynamicStoreCopyConsoleUser(None, None, None) or [None])[0]; username = [username,""][username in [u"loginwindow", None, u""]]; sys.stdout.write(username + "\n");')

# Grab the UUID of the Open Directory user
#UUID=$(/usr/sbin/diskutil apfs listCryptoUsers / | awk '/Open Directory/{if (a && a !~ /Open Directory/) print a;} {a=$2}')

# Grab the UUID of user $SSO
UUID=$(/usr/bin/fdesetup list -extended | awk -v sso=$SSO '$0 ~ sso {print $1}')

# Change FileVault passphrase for $UUID
/usr/sbin/diskutil apfs changePassphrase / -user $UUID


if [[ $? == 0 ]]; then
	echo "\n FileVault password updated successfully. Please reboot your Mac"
	exit 0
else
	echo "\n Something happened and FileVault password couldn't be changed. See Emilio."
	exit 1
fi