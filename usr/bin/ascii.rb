#! /usr/bin/env ruby

require 'set'

def clear
  $stdout.write "\r\e[0K"
  $stdout.flush
end

Start = "\342\223\223".bytes.to_a

chars = Set.new

begin
  x0 = 223
  y0 = 223
  x1 = 500
  y1 = 500
  for i in (x0..x1)
    for j in (y0..y1)
      a = Start[0]
      b = Start[1] + i
      c = Start[2] + j
      z = [a, b, c]
      s = z.join(':')
      z = z.pack('c*')
      chars << z
      #$stdout.write s + " "
      #$stdout.flush
    end
  end
rescue Interrupt => ex
end

chars = chars.to_a

puts chars.join(' : ')

#clear
