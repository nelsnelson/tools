#! /usr/bin/env ruby

def usage
  puts "Usage: informc <inform project directory|inform file>"
  exit
end

def die(s)
  puts s
  exit
end

usage if ARGV.empty?

unless File.exists? "inform_library"
  puts "The Inform libraries must be in a directory named 'inform_library' " +
       "and be available in your current working directory."
  exit
end

target = ARGV.shift
path_to_target = File.expand_path(target)
cwd = Dir.pwd
die "No such file: #{path_to_target}" unless File.exists? path_to_target
zcode = "#{target.split('.').first}.z5"
cmd = "inform630_macosx +inform_library611 $huge #{path_to_target} #{zcode}"
puts "Compiling #{target} into #{zcode}..."
puts `#{cmd}`.strip
