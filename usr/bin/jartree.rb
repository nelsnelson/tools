#! /usr/bin/env ruby

require 'set'
require 'optparse'

class Tree
  attr_accessor :data, :children
  def initialize(data)
    @data, @children = data, Set.new
  end

  def accept(visitor)
    visitor.visitData(self, data)
    children.each { |child| child.accept(visitor.visitTree(child)) }
  end

  def child(o)
    if o.is_a? Tree
      child = o
      children.add(child)
      return child
    else
      data = o
      for child in children
        if child.data == data
          return child
        end
      end

      return child(Tree.new(data))
    end
  end
end

class PrintIndentedVisitor
  attr_accessor :indent, :level

  Elbow  = "\342\224\224"
  Limb   = "\342\224\200"
  Branch = "\342\224\234"

  def initialize(indent=0, level=0)
    @indent, @level = indent, level
  end

  def visitTree(tree)
    PrintIndentedVisitor.new(indent + 4, level)
  end

  def visitData(parent, data)
    return if level and level < indent/2
    padding = "%#{indent}s" % [ "#{Elbow}#{Limb}#{Limb} " ]
    puts "%s%s" % [ padding, data ]
  end
end

script_name = File.basename __FILE__

options = Hash.new
OptionParser.new do |opt|
  opt.banner = "Usage: #{script_name} [options] <jar>\n\n"
  opt.on('-L', '--level=level', 'Maximum level of branches to display.') { |v| options[:level] = v }
  opt.on_tail("-?", "--help", "Show this message") do
    puts opt
    exit
  end
  opt.on_tail('-v', '--version', "Show version") do
    puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0'
    exit
  end
  begin
    opt.parse!(ARGV)
  rescue OptionParser::InvalidOption => e
    puts opt
    exit
  end
end
options[:jar] = ARGV.shift

paths = `jar -tf #{options[:jar]}`.split

forest = Tree.new(options[:jar])
current = forest

for tree in paths
  root = current

  for data in tree.split(/\//)
    current = current.child(data)
  end

  current = root
end

forest.accept(PrintIndentedVisitor.new(0, options[:level]))
