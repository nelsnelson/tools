#! /usr/bin/env ruby

require 'open-uri'

search = ARGV.length == 1 ? ARGV.first.gsub(/\s/, '+') : ARGV.join('+')

url = "http://www.youtube.com/results?search_query=#{search}&search_sort=video_most_viewed"
result = open(url, 'User-Agent' => 'ruby-wget').read

puts result.grep(/\<a.*href\=\"\/watch\?v\=([^\"\&]+).*\>.*\<img.*title\=\"([^\"]+)\"/) { [ $1, $2 ] }.first

