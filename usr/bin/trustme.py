#!/usr/bin/env python

import sys
import os

"""
This is a script for setting up the public key files
on servers so that users don't have to type their
passwords over and over each time they login. 
"""

algorithm = "dsa"
username = os.getlogin()
password = "" 
hostname = ""

ssh_directory = "~/.ssh"
keyfile = ssh_directory + "/id_" + algorithm
public_keyfile = "id_" + algorithm + ".pub"
authorized_keys_file = ssh_directory + "/authorized_keys"

class foo:
    def trust(self, username, hostname):
        if not os.path.exists(keyfile):
            os.system('ssh-keygen -t ' + algorithm + ' -P \"\" -N \"\" -f ' + keyfile)
        print "Sending public key to " + hostname 
        os.system('scp ' + ssh_directory + "/" + public_keyfile + ' ' + hostname + ':~')

        print "Requesting trust from " + hostname
        self.remote('mkdir -p ' + ssh_directory + '; ' + 
        'chmod 700 ' + ssh_directory + '; ' + 
        'cat ~/' + public_keyfile + ' >> ' + authorized_keys_file + '; ' + 
        'chmod 644 ' + authorized_keys_file + '; ' + 
        'rm -f ~/' + public_keyfile, username, hostname)

    def remote(self, command, username = '', hostname = ''):
        os.system('ssh ' + username + '@' + hostname + ' "' + command + '"')

if __name__ == '__main__':
    if len(sys.argv) > 2:
        username = str(sys.argv[1])
        hostname = str(sys.argv[2])
    elif len(sys.argv) == 2:
        hostname = str(sys.argv[1])
    else:
        print "Usage: ./trustme.py [username] <hostname>"
        sys.exit() 
    foo().trust(username, hostname)

