#! /usr/bin/env ruby

require 'rubygems'
require 'nokogiri'

file = ARGV.shift
name = ARGV.shift

exit unless file

doc = Nokogiri::XML(File.open(file))

proj_name = doc.xpath("//plist/dict/array/dict/key").search("[text()*='name']").first.next_element
s = proj_name.text

proj_name.content = name

doc.xpath("//plist//dict//key").each do |el|
  el.content = el.content.gsub(/^#{s}/, name)
end
doc.xpath("//plist//dict//string").each do |el|
  el.content = el.content.gsub(/^#{s}/, name)
end

File.open("#{name}.tmproj", "w") { |f| doc.write_xml_to f }


