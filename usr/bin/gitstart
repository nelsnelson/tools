#! /usr/bin/env ruby

require 'optparse'

git = `which git`

unless git
  puts "You must install git"
  exit 0
end

script_name = File.basename __FILE__

options = {}
OptionParser.new do |opt|
  opt.banner = "Usage: #{script_name} <directory to version control> [name of remote repository] [options]\n\n"
  opt.on('-o', '--origin=origin', 'Specify the exact origin of the existing repository.  I.e.: "git@github.com:nelsnelson/repo.git"') { |v| options[:origin] = v }
  opt.on('-h', '--host=host', 'Specify the host server address of the remote repository.') { |v| options[:host] = v }
  opt.on('-n', '--name=remote_repository', '--remote-name=remote_repository', 'Specify the name of the remote repository.') { |v| options[:remote_name] = v }
  opt.on_tail("-?", "--help", "Show this message") do
    puts opt
    exit
  end
  opt.on_tail('-v', '--version', "Show version") do
    puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0'
    exit
  end


  begin
    opt.parse!(ARGV)
  rescue OptionParser::InvalidOption => e
    puts opt
    exit
  end
end

options[:target] = ARGV.shift if not ARGV.empty? and not options[:target]
options[:name] = ARGV.shift if not ARGV.empty? and not (options[:name] or options[:remote_name])

target = options[:target] 

unless target
  puts "Which directory would you like to version control?"
  exit
end

host = options[:host] || "nelsnelson.org"

repo = options[:name] || options[:remote_name] || target

origin = options[:origin] || "git@#{host}:#{repo}.git"
user   = options[:user] || "git"
url    = "git@#{host}:#{repo}"

puts "Creating remote repository: #{repo}.git"
`ssh git@#{host} 'mkdir -p #{repo}.git; cd #{repo}.git; git init --bare;'`

# push existing local source into a remote repository
local_steps = %Q{
mkdir -p #{target} ;
cd #{target} ;
echo "Initializing local copy: #{target}" ;
git init ;
echo "Adding remote origin: #{origin}" ;
git remote add origin "#{origin}" ;
echo "Configuring local copy" ;
cat .git/config ;     # => [core]
                      # =>     repositoryformatversion = 0
                      # =>     filemode = true
                      # =>     bare = false
                      # =>     logallrefupdates = true
                      # => [remote "origin"]
                      # =>     url = #{url} 
                      # =>     fetch = +refs/heads/*:refs/remotes/origin/*
echo "Adding contents" ;
git add -A
#[ $(ls | wc -l) == 0 ] && git add . || git add * ;
echo "Comitting" ;
git commit --allow-empty -am 'initial commit' ;
#git branch -a ;       # => * master
echo "Pushing origin master" ;
git push origin master ;
git branch -a ;       # => * master
                      # =>   origin/master
git config branch.master.remote origin ;
git config branch.master.merge refs/heads/master ;
git config remote.origin.url #{url} ;
git config remote.origin.fetch +refs/heads/*:refs/remotes/origin/* ;
git config --global push.default matching ;
}
`#{local_steps}`

exit

