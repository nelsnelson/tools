#! /usr/bin/env ruby

require 'optparse'

OPTIONS = {}
@usage = OptionParser.new do |opt|
  opt.banner = "Usage: #{$0} [host[:port]] [options...]\n\n"
  opt.on('-h', '--host=host', 'Hostname of the server from which the SSL certificate will be imported.') { |v| OPTIONS[:host] = v }
  opt.on('-p', '--port=port', 'Port of the server from which the SSL certificate will be imported.') { |v| OPTIONS[:port] = v }
  opt.on('-f', '--file=file', 'The file containing the SSL certificate to be imported.') { |v| OPTIONS[:cert] = v }
  opt.on('-K', '--java-keystore=keystore', 'The global Java keystore file into which an alias of the certificate will be inserted.') { |v| OPTIONS[:java_keystore] = v }
  opt.on('-k', '--keystore=keystore', 'The local keystore file into which an alias of the certificate will be inserted.') { |v| OPTIONS[:keystore] = v }
  opt.on('-b', '--keytool=keytool', 'The keytool binary.') { |v| OPTIONS[:keytool] = v }
  opt.on('-d', '--debug', 'Do you want to see shell commands printed out during execution?') { |v| OPTIONS[:debug] = true }
  opt.on_tail("-?", "--help", 'Show this message.') do
    puts opt
    exit
  end
  opt.on_tail('-v', '--version', "Show version.") do
    puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0'
    exit
  end

  begin
    opt.parse!(ARGV)
  rescue OptionParser::InvalidOption => e
    puts opt
    exit
  end
  unless ARGV.empty?
    expected_host_and_port = ARGV.shift
    (host, port) = expected_host_and_port.split(':')
    OPTIONS[:host] ||= host
    OPTIONS[:port] ||= (port || 443)
  end
end

def main
  run
rescue Interrupt, SystemExit
rescue StandardError => e
  puts e
  puts e.backtrace
  exit 1
ensure
  cleanup
end

def run
  OPTIONS[:keytool] ||= `which keytool`.strip
  if root?
    paths = `find /usr/java -name keytool`.strip
    unless paths.empty? or paths.split.grep(/Find\:.*Permission denied/)
      OPTIONS[:keytool] = paths.split.first
    end
  end
  unless OPTIONS[:host]
    unless File.exists? 'LATEST.jks'
      usage "Default action is to import local keystore LATEST.jks.",
            "Provide path to LATEST.jks or another keystore with -k or --keystore."
    end
  end
  raise "Cannot find keytool binary" unless File.exists? OPTIONS[:keytool]
  OPTIONS[:java_keystore] ||= if ENV['JAVA_HOME']
    `find #{ENV['JAVA_HOME']} -name cacerts`.strip.split.first
  elsif File.exists? '/usr/libexec/java_home'
    `find $(/usr/libexec/java_home) -name cacerts`.strip.split.first
  else
    '/System/Library/Java/Support/CoreDeploy.bundle/Contents/Home/lib/security/cacerts'
  end
  unless File.exists? OPTIONS[:java_keystore]
    raise "Could not find the global java keystore file (usually cacerts): please specify using -K or --java-keystore"
  end

  if OPTIONS[:host]
    OPTIONS[:keystore] ||= "#{OPTIONS[:host]}.jks"
    root_prompt unless can_sudo?
    host = OPTIONS[:host]
    port = OPTIONS[:port] || 443
    a = fetch_cert host, port
    cert_file = host + '.crt'
    File.open(cert_file, 'w') { |f| f << a }
    puts create_keystore(host)
    puts delete_alias(host) if alias_exists?(host)
    puts import_keystore(host)
    puts "Confirming alias #{host} is in keystore file: #{OPTIONS[:java_keystore]}"
    puts confirm_alias(host)
  else
    OPTIONS[:keystore] ||= "LATEST.jks"
    return unless yes? "Import local keystore #{OPTIONS[:keystore]}?"
    root_prompt unless can_sudo?
    certs = Dir['*.crt']
    for f in certs
      host = f[/(.*)\..*/, 1]

      if File.exists?(OPTIONS[:keystore])
        puts delete_alias(host, OPTIONS[:keystore]) if alias_exists?(host, OPTIONS[:keystore])
        puts add_alias(host, OPTIONS[:keystore])
      else
        puts create_keystore(host, OPTIONS[:keystore])
      end
    end
    puts import_keystore(host, OPTIONS[:keystore])
    puts "Confirming aliases are in keystore file: #{OPTIONS[:java_keystore]}"
    for f in certs
      puts confirm_alias(f[/(.*)\..*/, 1])
    end
  end
end

def cleanup
  if OPTIONS[:host]
    host = OPTIONS[:host]
    cert_file = host + '.crt'
    delete_keystores
    File.delete cert_file if File.exist? cert_file
  end
end

def usage(*messages)
  messages.each do |m| puts m end and puts if messages
  puts @usage if @usage
  exit
end

def yesorno?(question='Continue?')
  ask(question, 'Y/n') =~ /y/i
end
alias :yes? :yesorno?

def no?(question)
  ask(question, 'y/N') =~ /n/i 
end

def ask(question, opts='y/N', default=opts.scan(/.*([A-Z]).*/).flatten.first)
  prompt "#{question} (#{opts})> ", {
    :constraints => opts.split(/\//),
    :default => default
  }
end

def prompt(prompt, opts={})
  constraints = opts[:constraints]
  while true
    out prompt
    response = gets.chomp
    return opts[:default] if response.empty?
    return response unless constraints
    if response =~ /(#{constraints.join('|')})/i
      return response
    else
      puts "Please respond with #{constraints[0..-2].join(', ')} or #{constraints[-1]}"
    end
  end
end

def out(s)
  $stdout.write s
  $stdout.flush
end

def root?
  Process.uid == 0
end

def can_sudo?
  `sudo -n uptime 2>&1 | grep "load" | wc -l`.strip.to_i > 0
end

def root_prompt
  `sudo -p \"Your password is required for admin access: \" echo`
  exit unless can_sudo?
end

def debug(s)
  puts "\n    $ #{s}\n\n" if OPTIONS[:debug]
end

def create_keystore(host, keystore="#{host}.jks")
  puts "Creating keystore #{keystore} with initial alias for #{host}"
  cmd = "#{OPTIONS[:keytool]} -importcert -noprompt -file #{host}.crt -keystore #{keystore} -storepass changeit -alias #{host}"
  debug cmd
  `#{cmd}`
end

def import_keystore(host, keystore="#{host}.jks", target_keystore=OPTIONS[:java_keystore])
  puts "Importing aliases in #{keystore}"
  cmd = "sudo #{OPTIONS[:keytool]} -importkeystore -srckeystore #{keystore} -destkeystore #{target_keystore} -srcstorepass changeit -deststorepass changeit -noprompt"
  debug cmd
  `#{cmd}`
end

def delete_keystores
  cmd = "find . -maxdepth 1 -type f \\( -iname \"*.jks\" ! -iname LATEST.jks \\) -delete"
  debug cmd
  `#{cmd}`
end

def fetch_cert(host, port)
  puts "Fetching certificate for https://#{host}/"
  cmd = "echo | /usr/bin/openssl s_client -connect #{host}:#{port} 2>&1"
  debug cmd
  result = `#{cmd}`
  raise "Failed to get certificate for #{host}" if result =~ /gethostbyname failure/
  cmd = "echo \"#{result}\" | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'"
  debug cmd
  `#{cmd}` 
end

def delete_alias(host, keystore=OPTIONS[:java_keystore])
  puts "Deleting old alias for #{host} from existing keystore file: #{keystore}"
  cmd = "sudo #{OPTIONS[:keytool]} -delete -trustcacerts -alias #{host} -keystore #{keystore} -deststorepass changeit -noprompt"
  debug cmd
  `#{cmd}`
end

def add_alias(host, keystore=OPTIONS[:java_keystore])
  puts "Adding new alias for #{host} to existing keystore file: #{keystore}"
  cmd = "sudo #{OPTIONS[:keytool]} -import -trustcacerts -alias #{host} -file #{host}.crt -keystore #{keystore} -deststorepass changeit -noprompt"
  debug cmd
  `#{cmd}`
end

def confirm_alias(host, keystore=OPTIONS[:java_keystore])
  unless alias_exists? host
    "Failed to add alias to #{keystore}"
  else
    "Confirmed alias for #{host} is in #{keystore}"
  end
end

def alias_exists?(host, keystore=OPTIONS[:java_keystore])
  cmd = "sudo #{OPTIONS[:keytool]} -list -keystore #{keystore} -deststorepass changeit | grep #{host}"
  debug cmd
  a = `#{cmd}`.strip
  return !(a.empty?)
end

main if __FILE__ == $0
