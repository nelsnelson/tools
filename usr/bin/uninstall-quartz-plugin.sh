#! /usr/bin/env bash

grails=`which grails`
[[ -z "${grails}" ]] && { echo "Please make your path include grails"; exit; }
[[ -n "${1}" ]] && target="${1}" || target=`pwd`
blank() { echo -ne "\r\033[K"; }
echo "Searching ${target} for grails quartz plugin specification..."
for f in ${target}/**/presentation/application.properties; do
    quartz_is_installed=`grep "plugins.quartz=" ${f}`
    presentation=`dirname ${f}`
    project=`dirname ${presentation}`
    if [ -z "${quartz_is_installed}" ]; then
        blank
        echo -n "The grails quartz plugin is not installed in ${project}"
    fi
    if [ -n "${quartz_is_installed}" ]; then
        echo "${quartz_is_installed}: ${presentation}"
        cd $presentation
        grails uninstall-plugin quartz
    fi
    cd $project
    status=`svn status | grep "presentation/application.properties"`
    if [ -n "${status}" ]; then
        svn update presentation/application.properties --accept=theirs-full
        svn ci presentation/application.properties -m "QA-1 Removing Quartz"
    fi
done
blank
echo "Done"
