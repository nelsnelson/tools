#! /usr/bin/env ruby

output = "/tmp/message.aiff"
home = ENV.fetch('HOME', '~/')

cmd = "pbpaste | say --voice Samantha --output-file=#{output}"

puts "#{cmd}"

result = `#{cmd}`.strip

input = output
output = `basename "#{input}" .aiff`.strip
output = "/tmp/#{output}.wav"

cmd = "rm -f \"#{output}\""

puts "#{cmd}"

result = `#{cmd}`.strip

cmd = "ffmpeg -v 0 -i \"#{input}\" -acodec pcm_s16le -ac 1 -ar 44100 \"#{output}\""

puts "#{cmd}"

result = `#{cmd}`.strip

input = output
output = `basename "#{input}" .wav`.strip
output = "#{home}/#{output}.mp3"

cmd = "rm -f \"#{output}\""

puts "#{cmd}"

result = `#{cmd}`.strip

cmd = "lame --silent -h -b 192 \"#{input}\" \"#{output}\""

puts "#{cmd}"

result = `#{cmd}`.strip

cmd = "rm -f \"#{input}\""

puts "#{cmd}"

result = `#{cmd}`.strip
