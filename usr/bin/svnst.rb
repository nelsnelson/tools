#! /usr/bin/env ruby

cmd = "svn status | grep -P '^(?=.{0,6}C)'"
conflicts = `#{cmd}`.strip

if conflicts.nil? || conflicts.empty?
  puts 'All good'
 else
  puts conflicts
end
