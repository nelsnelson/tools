#! /usr/bin/env ruby

require 'optparse'

script_name = File.basename __FILE__

options = {}
OptionParser.new do |opt|
  opt.banner = "Usage: #{script_name} [directory] [options]\n\n"
  opt.on('-o', '--output-document=file', 'Save consolidated file as... (default is the name of the specified or current directory)') { |v| options[:file] = v }
  opt.on('-s', '--file-type=type', 'Consolidated file-type') { |v| options[:file_type] = v }
  opt.on_tail("-?", "--help", "Show this message") do
    puts opt
    exit
  end
  opt.on_tail('-v', '--version', "Show version") do
    puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0' 
    exit
  end
  
  begin
    opt.parse!(ARGV)
  rescue OptionParser::InvalidOption => e
    puts opt
    exit
  end
end

options[:target] = ARGV.shift if not ARGV.empty?

target = options[:target] || Dir.getwd
save_file = options[:file] || target

content = ""

excluded_dirs  = [ ".git", ".svn", ".idea", "cobertura" ]
excluded_types = [ "png", "gif", "jpg", "tiff", "ico", "swf", "data", "class", "war", "jar", "log" ]

# Construct the find command

cmd  = "find '#{target}' "

cmd += "\\( "

for dir in excluded_dirs
  cmd += "! -regex '.*/\\#{dir}.*' "
end

for type in excluded_types
  cmd += "! -regex '.*\.#{type}' "
  cmd += "! -regex '.*\.#{type.upcase}' "
end

cmd += "\\)"

#puts "The find command is: #{cmd}"

# Gather the statistics on the collected files

puts `date`
puts ""
if File.directory?(target)
  puts "Scanning directory #{target}"
else
  puts "Scanning file #{target}"
end

results = `#{cmd}`.split("\n").sort  # Execute the find command

puts ""

counter=0

for f in results
  if File.exists? f and not File.directory? f
    counter += 1
    section = "### #{counter}.) #{f}"
    content << "\n"
    content << section
    content << "\n"
    content << "### #{'-'*section.length}\n\n"
    content << File.new(f).readlines
    print "\r#{counter} source code files "; $stdout.flush()
  end
end

# Save the new content

puts content

