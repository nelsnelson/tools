#! /usr/bin/env ruby

class Program
  def initialize
    run
  rescue Interrupt
  end

  def options
    @options ||= parse_args
  end
end

class Main < Program
  def initialize
    unless options[:dir] and File.exists? options[:dir]
      puts "You must specify a directory containing a subversion project from which you will be merging."
      exit
    end
    unless options[:target] and File.exists? options[:target]
      puts "You must specify a directory into which to merge."
      exit
    end
    super
  ensure
    clear
  end

  def run
    dir = options[:dir]
    target = options[:target]
    from_log = `svn log --stop-on-copy #{dir}`
    from_revision = from_log.grep(/^r([0-9]+)\s\|/) { $1 }.first
    from_issue = from_log.grep(/^(\w+\-[0-9]+)\s/) { $1 }.first
    from_title = from_log.grep(/^\w+\-[0-9]+\s(.*)$/) { $1 }.first
    issue = options[:issue] || from_issue
    target_dir = options[:target] || dir.gsub(/\-trunk/, '') + issue.gsub(/[\D]/, '')
    commit_title = options[:title] || from_title
    commit_title = commit_title.split if commit_title.respond_to? :split
    branch_title = commit_title.join '-'
    orig_url = `svn info #{dir}`.grep(/URL: (.*)/) { $1 }.first
    orig_url_parts = orig_url.split(/(\/branches\/|\/trunk\/?)/)
    base_url = orig_url_parts.first
    date = Time.now.strftime('%Y-%m-%d')
    orig_target_url = `svn info #{target}`.grep(/URL: (.*)/) { $1 }.first
    target_url = orig_target_url
    branch_url = "#{base_url}/branches/#{issue}-#{branch_title}-#{date}"
    orig = orig_url =~ /\/trunk\/?/ ? "trunk" : orig_url_parts.last
    message = options[:message] || "Merge from #{orig} to #{target_dir}.\n"
    message = "#{issue} #{commit_title.join ' '}\n\n#{message}"
    dry_run = options[:dry_run] ? '--dry-run' : ''

    unless target_dir =~ /([\d]{4}-[\d]{2}-[\d]{2})/
      target_dir = "#{target_dir}-#{date}"
    end
    
    puts "Dry-running" if options[:dry_run]

    unless options[:skip_verification]
    puts "Merging from: #{dir} (#{orig_url})"
    puts "Merging into: #{target_dir} (#{target_url})"
    yesorno
    puts "Okay."
    puts "Merge message is:"
    puts "#{message}\n"
    yesorno
    puts "Original directory is:    #{dir}"
    puts "Merge directory will be: #{target_dir}" 
    yesorno
    end

    progress("Merging") { cmd = "svn merge #{dry_run} #{orig_url} -r#{from_revision}:HEAD #{target_dir}"; puts cmd }
    #progress("Checking in #{target_dir} to #{target_url}") { `svn checkin #{target_dir} -m "#{message}"--quiet` }
  end
end

def parse_args
  require 'optparse'

  script_name = File.basename __FILE__

  options = {}
  OptionParser.new do |opt|
    opt.banner = "Usage: #{script_name} <dir> [title] [options...]\n\n"
    opt.on('-d', '--dir=dir', 'Directory containing the branch from which to merge') { |v| options[:dir] = v }
    opt.on('-o', '--output-directory=dir', 'Directory into which you will be merging') { |v| options[:target] = v }
    opt.on('-t', '--title=title', 'Title information') { |v| options[:title] = v }
    opt.on('-m', '--message=message', 'Commit message') { |v| options[:message] = v }
    opt.on('-n', '--name=name', 'Name of the branch') { |v| options[:name] = v }
    opt.on('-i', '--issue=issue', 'Issue tracking number or tag') { |v| options[:issue] = v }
    opt.on('--dry-run', 'Dry run') { |v| options[:dry_run] = true }
    opt.on('-q', '--skip-verification', 'Skip verification') { |v| options[:skip_verification] = v }
    opt.on_tail("-?", "--help", "Show this message") do
      puts opt
      exit
    end
    opt.on_tail('-v', '--version', "Show version") do
      puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0'
      exit
    end

    begin
      opt.parse!(ARGV)
    rescue OptionParser::InvalidOption => e
      puts opt
      exit
    end
  end

  options[:dir] ||= ARGV.shift if not ARGV.empty?
  options[:target] ||= ARGV.shift if not ARGV.empty?
  while not ARGV.empty?
    options[:title] ||= []
    options[:title] << ARGV.shift
  end
  options
end

def yesorno
  response = prompt "Continue? (Y/n)> ", { :constraints => [ 'y', 'n' ] }
  exit if response =~ /n/
end

def prompt(prompt, options={})
  constraints = options[:constraints]
  while true
    put prompt
    response = gets.chomp
    return response unless constraints
    if response =~ /(#{constraints.join('|')})/i
      return response
    else
      puts "Please respond with #{constraints[0..-2].join(', ')} or #{constraints[-1]}"
    end
  end
end

def progress(message, debug=false, &block)
  put message
  t = Thread.new do
    a = [ ".", "..", "..."]
    i=0
    while true
      sleep 1
      status " %s%s" % [ message, a[i] ]
      i+=1
      i%=3
    end
  end
  output = yield block
  put output if debug
  t.kill
end

def put(s)
  $stdout.write s
  $stdout.flush
end

def clear
  put "\r\e[0K"
end

def status(s)
  clear
  put "%s\r" % s
end

Main.new if __FILE__ == $0

