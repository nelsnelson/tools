#! /usr/bin/env ruby

def put(s)
  $stdout.write s
  $stdout.flush
end

def yesorno
  response = prompt "Continue? (Y/n)> ", { :constraints => [ 'y', 'n' ] }
  exit if response =~ /n/
end

def prompt(prompt, options={})
  constraints = options[:constraints]
  while true
    put prompt
    response = $stdin.gets.chomp
    return response unless constraints
    if response =~ /(#{constraints.join('|')})/i
      return response
    else
      puts "\nPlease respond with #{constraints[0..-2].join(', ')} or #{constraints[-1]}"
    end
  end
end

exit if ARGV.empty?

@name_of_this_script = File.basename $0
@results = `ps aux | grep -v "#{@name_of_this_script}" | grep -v "grep" | grep #{ARGV.join(" ")}`.split("\n")
@pids = @results.inject({}) { |m,v| a = v.split(' '); m[a[1]] = a.last; m }
for pid,process in @pids do
  next if prompt("Are you sure you want to kill the #{process} (#{pid}) process (Y/n)? ", { :constraints => [ 'y', 'n' ] }) =~ /n/
  `kill -9 #{pid}`
end

