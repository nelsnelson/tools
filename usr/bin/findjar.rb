#! /usr/bin/env ruby

def clear
  $stdout.write "\r\e[0K"
  $stdout.flush
end

def status(s)
  clear
  $stdout.write "%s\r" % s
  $stdout.flush
end

query = ARGV.join(' ')
found = false

begin
  jars = `find . -name "*.jar" 2>&1| grep -ve ".*: Permission denied$"`.split
  n = jars.length

  jars.each_with_index do |j, i|
    status "%10s of %s: %s" % [ i, n, j ]
    
    a = `jar -tf #{j} | grep "#{query}"`.strip
    next if a.empty?
    found = true
    clear
    puts " #{j}:"
    puts a
  end
rescue Interrupt => ex
end

clear

puts "Not found: #{query}" unless found
