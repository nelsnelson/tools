#!/bin/bash

# author: Matt Harasymczuk, http://www.matt.harasymczuk.pl
# license: Apache 2.0 License
# since: 2011-04-30
#
# tune - music player, plays your music in random order
# uses mplayer to play, and growlnotify to pop-up info
# about tunes currently played
#
# in order to kill application and exit from playing
# hit ctrl-c two times, fast, or kill the process



# music dir and file names (case insensitive) to scan
DIR=~/Music
NAME='*.mp3'



# do not modify anything below
# unless you know what are you doing

# executables
MPLAYER=`which mplayer`
GROWL=`which growlnotify`

if [ ! $MPLAYER ]; then
    echo "ERROR: mplayer not found on your system"
    echo 'ERROR: install mplayer, or/and add to the $PATH'
    exit 1
fi

if [ ! $GROWL ]; then
    echo "WARNING: growl not found on your system"
    echo "WARNING: will not display popup info"
fi

# command line options
MPLAYER="$MPLAYER"
if [ ! -z $GROWL ]; then
    GROWL="$GROWL --n mplayer --image /Users/haras/Pictures/_random/random/music-note.png"
fi

# script home
TUNE=~/.tune
PLS="$TUNE/playlist.pls"
LOG="$TUNE/history.log"

# make script home if not exists
if [ ! -e $TUNE ]; then
    mkdir $TUNE
fi

# populate playlist
find $DIR -iname "$NAME" > $PLS

# enter the infinite loop
while true; do
    # generate some random seed
    # and take random line from file
    RAND=`od -d -N2 -An /dev/urandom`
    LINES=`cat "$PLS" | wc -l`
    LINE=$(( RAND % LINES + 1 ))

    # file to be played
    PLAY=`head -$LINE $PLS | tail -1`

    # split filename by /
    OLD_IFS=$IFS
    IFS='/'
    DATA=( $PLAY )
    ARTIST=${DATA[7]}
    ALBUM=${DATA[8]}
    FILENAME=${DATA[9]}
    IFS=$OLD_IFS

    # wait 10 secunds with growl, let it start playing
    [[ ! -z $GROWL ]] && sleep 10 && $GROWL -t "$ARTIST" -m "$ALBUM"$'\n\n'"$FILENAME"&
    clear

    # log info format: [date] filename
    echo [`date +'%F %H:%M:%S'`] $PLAY >> $LOG
    
    # play :}
    $MPLAYER "$PLAY"

    # display once again, maybe you liked it and want whats the name ;}
    [[ ! -z $GROWL ]] && $GROWL -t "Last played" -m "$ARTIST"$'\n\n'"$ALBUM"$'\n\n'"$FILENAME"
 
    # wait one second
    # tunes do not overlaps and
    # you are able to kill script
    # using ctrl-c key combo
    sleep 1
done


