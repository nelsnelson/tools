#! /usr/bin/env ruby

@os_name = begin
  require 'sys/uname'
  Sys::Uname.sysname
rescue LoadError => ex
  `uname -s`.strip
end

# Cross-platform way of finding an executable in the $PATH.
#
#   which('ruby') #=> /usr/bin/ruby
def which(cmd)
  exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
  ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
    exts.each { |ext|
      exe = "#{path}/#{cmd}#{ext}"
      return exe if File.executable? exe
    }
  end
  return nil
end

lod = "\340\262\240_\340\262\240"
case @os_name
 when 'Darwin'
  IO.popen('pbcopy', 'w').write lod if which 'pbcopy'
 when 'Linux'
  if which 'xclip'
  else
    puts "Cannot find xclip.  Try running\n\n$ sudo apt-get install xclip"
  end
end
puts "\n#{lod}\n\n"

