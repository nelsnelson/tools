#! /usr/bin/env ruby

module PrimeNumbers
  def prime?(n)
    return false if n <= 1
    return true if n <= 3
    return false if n % 2 == 0 or n % 3 == 0
    i = 5
    while (i * i) <= n do
      return false if n % i == 0 or n % (i + 2) == 0
      i = i + 1
    end
    true
  end

  def main
    s = ARGV.shift
    unless s.match(/\A[+-]?\d+?(\.\d+)?\Z/).nil?
      n = s.to_i
      if prime? n
        puts "#{n} is a prime number"
        exit
      end
    end
    puts "#{s} is not a prime number"
  end
end # module PrimeNumbers

Object.new.extend(PrimeNumbers).main
