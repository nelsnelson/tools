#! /usr/bin/env python

import re
import os
import sys
import urllib
import httplib
import urllib2
import tempfile

sys.argv.pop(0)
words = sys.argv
ogg_urls = {} 
for word in words:
    word = word.replace('\"', '')

    print 'Getting definition for \'%s\'' % word 

    try:
        wiktionary_url = 'http://fr.wiktionary.org/wiki/%s' % word
        #wiktionary_url = 'http://commons.wikimedia.org/wiki/File:Tromboon-sample.ogg'

        host = 'fr.wiktionary.org'
        request = urllib2.Request(wiktionary_url)
        request.add_header('User-Agent', 'Python Pronunciation Command Line Script/1.0 +http://nelsnelson.org/') 
        response = urllib2.urlopen(request)
        content = response.read()

        results = re.findall('\/\/[\S]*\.ogg', content)
        for match in results:
            ogg_urls[word] = "http:" + match
    except Exception, e:
        print e

if len(ogg_urls.values()) == 0:
    print 'Sorry, could not find definition for \'%s\'' % ' '.join(words)
    sys.exit()

#vlc = '/Applications/VLC.app/Contents/MacOS/VLC access %s -vvv -I dummy --audio --no-interact --play-and-exit'
vlc = 'DYLD_LIBRARY_PATH=/Users/nels.nelson/Applications/VLC.app/Contents/MacOS/lib/ /Users/nels.nelson/.local/bin/vlc --quiet --intf=rc --play-and-exit %s vlc://quit'
os.system(vlc % (' '.join(ogg_urls.values())))

