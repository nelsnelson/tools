#! /usr/bin/env bash

[[ $EUID -ne 0 ]] && { echo "You are not root"; exit 1; }

JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Home

REMHOST=$1
REMPORT=${2:-443}
CERTFILE="${REMHOST}.crt"
KEYSTORE="${REMHOST}.jks"

echo | openssl s_client -connect ${REMHOST}:${REMPORT} 2>&1 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > ${CERTFILE}
keytool -importcert -noprompt -file ${CERTFILE} -keystore ${KEYSTORE} -storepass changeit -alias ${REMHOST} 
keytool -importkeystore -srckeystore ${KEYSTORE} -destkeystore $JAVA_HOME/lib/security/cacerts -srcstorepass changeit -deststorepass changeit -noprompt

rm -f ${CERTFILE}
rm -f ${KEYSTORE}

