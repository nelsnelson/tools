#! /usr/bin/env ruby

class Program
  attr_accessor :options
end

class Main < Program
  def run
    dir = options[:dir] || Dir.getwd

    branch_info = `svn log --stop-on-copy #{dir}`.grep(/^([\w]{3}-[\d]{4}) (.*)$/) { { :jira => $1, :title => $2 } }.last
    message = "#{branch_info[:jira]} #{branch_info[:title]}\n\n${message}\n"

    puts `svn commit -m "#{message}"`
  end
end

def parse_args
  require 'optparse'

  script_name = File.basename __FILE__

  options = {}
  OptionParser.new do |opt|
    opt.banner = "Usage: #{script_name} [dir] [message] [options...]\n\n"
    opt.on('-d', '--dir=dir', 'Directory containing subversion working checkout.  Default is current working directory.') { |v| options[:dir] = v }
    opt.on('-m', '--message=message', 'The commit message.  Default is "Quick saving progress.".') { |v| options[:message] = v }
    opt.on_tail("-?", "--help", "Show this message") do
      puts opt
      exit
    end
    opt.on_tail('-v', '--version', "Show version") do
      puts defined?(SCRIPT_VERSION) ? SCRIPT_VERSION : '1.0'
      exit
    end

    begin
      opt.parse!(ARGV)
    rescue OptionParser::InvalidOption => e
      puts opt
      exit
    end
  end

  options[:dir] = ARGV.shift if not ARGV.empty?
  while not ARGV.empty?
    options[:message] ||= []
    options[:message] << ARGV.shift
  end
  options
end

def yesorno
  response = prompt "Continue? (Y/n)> ", { :constraints => [ 'y', 'n' ] }
  exit if response =~ /n/
end

def prompt(prompt, options={})
  constraints = options[:constraints]
  while true
    put prompt
    response = gets.chomp
    return response unless constraints
    if response =~ /(#{constraints.join('|')})/i
      return response
    else
      puts "Please respond with #{constraints[0..-2].join(', ')} or #{constraints[-1]}"
    end
  end
end

def progress(message, &block)
  put message
  t = Thread.new do
    a = [ ".", "..", "..."]
    i=0
    while true
      sleep 1
      s = "\r#{message}#{a[i]}"
      put s + ' '*(80-s.length)
      i+=1
      i%=3
    end
  end
  yield block
  puts "\r" + ' '*80
  t.kill
end

def put(s)
  $stdout.write s; $stdout.flush
end

if __FILE__ == $0
  begin
  main = Main.new
  main.options = parse_args
  main.run
  rescue Interrupt
    puts ""
  end
end
