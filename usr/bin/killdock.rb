#! /usr/bin/env ruby

puts `is-mac-os`

@name_of_this_script = File.basename $0
@results = `ps aux | grep -v "#{@name_of_this_script}" | grep -v "grep" | grep Dock`.split("\n").map(&:strip)
@pids = @results.inject({}) { |m,v| a = v.split(' '); m[a[1]] = a.last; m }
for pid,process in @pids do
  cmd = "kill -9 #{pid}"
  #puts "About to execute command: `#{cmd}`"
  `#{cmd}`
end

