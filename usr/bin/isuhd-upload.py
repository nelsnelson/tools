#!/usr/bin/env python

#A Python script to upload to YouTube from iShowU HD http://www.shinywhitebox.com/
#See http://code.google.com/apis/youtube/developers_guide_protocol.html
#Licensed via the BSD license - http://www.opensource.org/licenses/bsd-license.php
#In short: we've provided this as an example upload script with iShowU HD and absolutely
#intend for you to be able to make other uploaders using it as a template. Hack away!
#But please leave some attribution to shinywhitebox

#Note that the YouTube API TOS compels us to not disclose our developer key
#...this is why it must be passed from a client application.

import urllib2
import httplib
import socket
import xml.dom.minidom
from urllib import urlencode
from cgi import parse_qs
from os.path import basename
from optparse import OptionParser
import sys

#Custom exceptions
class AuthenticationException(Exception):
    pass

class KeywordException(Exception):
    pass

#resources
boundary_string="isuhd-youtube-upload"

XML='''
<?xml version="1.0"?>
<entry xmlns="http://www.w3.org/2005/Atom"
  xmlns:media="http://search.yahoo.com/mrss/"
  xmlns:yt="http://gdata.youtube.com/schemas/2007">
  <media:group>
    <media:title type="plain">%s</media:title>
    <media:description type="plain">
      %s
    </media:description>
    <media:category
      scheme="http://gdata.youtube.com/schemas/2007/categories.cat">%s
    </media:category>
    <media:keywords>%s</media:keywords>
  </media:group>
</entry>
'''

#calls
class HTTPCustomConnectionHandler(urllib2.HTTPHandler):
    '''An HTTP handler that uses a custom HTTPConnection subclass'''
    def http_open(self, req):
        return self.do_open(HTTPProgressUploadConnection, req)

class HTTPProgressUploadConnection(httplib.HTTPConnection):
    '''An HTTP connection that provides feedback on how our upload is going'''
    chunk_length=16384;
    def request(self,method,url,body,headers):
        return httplib.HTTPConnection.request(self,method,url,body,headers)
    
    def send(self, str):
        '''Overloaded sending 'str' - dump over 16K at a time and report on progress'''
        if self.sock is None:
            if self.auto_open:
                self.connect()
            else:
                raise NotConnected()
        try:
            loc=0
            while loc<len(str):
                if loc+self.chunk_length>len(str): add_length=len(str)-loc
                else: add_length=self.chunk_length
                self.sock.sendall(str[loc:loc+add_length])
                loc+=add_length;
                if len(str)>self.chunk_length:
                    print "Progress: %d/%d" % (loc,len(str))
                    sys.stdout.flush() #IMPORTANT! progress indicator will not update if you don't flush stdout
        except socket.error, v:
            if v[0] == 32:      # Broken pipe
                self.close()
            raise

class HTTP201Handler(urllib2.HTTPDefaultErrorHandler):
    '''201's are OK because that's what YouTube returns to indicate that it's all gone well.'''
    def http_error_201(self, req, fp, code, msg, headers):
        return fp
    
def list_categories(locale):
    '''Query YouTube for all the categories and their localised text equivalents.'''
    file=urllib2.urlopen("http://gdata.youtube.com/schemas/2007/categories.cat?hl="+locale)
    reply_object=xml.dom.minidom.parse(file)
    for node in reply_object.childNodes[0].childNodes:
        print "%s=%s" % (node.getAttribute('term'),node.getAttribute('label'))

def auth_token(username,password):
    '''Returns a YouTube authentication token.'''
    try:
        server_response=urllib2.urlopen("https://www.google.com/youtube/accounts/ClientLogin",
                               urlencode({"Email":username,"Passwd":password,"service":"youtube","source":"iShowU HD"}))
        result=parse_qs(server_response.readline())
    except urllib2.HTTPError,e:
        if e.code==403: raise AuthenticationException
        else: raise
    return result['Auth'][0]
    
def upload(developer,username,auth_token,filename,title,description,category,keywords):
    '''Uploads a file to YouTube using the username and auth token provided. Pass keywords as a list.'''
    #Validate the keywords list
    for keyword in keywords:
        if ' ' in keyword: raise KeywordException
        if len(keyword)>25: raise KeywordException
    if len(','.join(keywords))>120: raise KeywordException
    #Start off by trying to open the file
    file=open(filename,"r")
    #Create the data we are going to pass
    #We do this by creating a list of strings then concatenating them together
    data_strings=[]
    data_strings.append("--"+boundary_string+"\nContent-Type: application/atom+xml; charset=UTF-8\n")
    data_strings.append(XML % (title,description,category,','.join(keywords)))
    data_strings.append("\n--"+boundary_string+"\nContent-Type: video/quicktime\nContent-Transfer-Encoding: binary\n\n")
    data_strings.append(file.read())
    data_strings.append("\n--"+boundary_string+"--\n")
    data=''.join(data_strings)
    #Create the request object
    rqst=urllib2.Request("http://uploads.gdata.youtube.com/feeds/api/users/%s/uploads" % username, data,
                 {'Authorization':'GoogleLogin auth='+auth_token,
                  'X-GData-Key':'key='+developer,
                  'Slug':basename(filename),
                  'Content-Type':'multipart/related; boundary="%s"' % boundary_string,
                  'Connection':'close'})
    #Finally upload it
    opener=urllib2.build_opener(HTTPCustomConnectionHandler,HTTP201Handler)
    return opener.open(rqst)

#parse command line
parser=OptionParser("Usage: %prog [options] file")
parser.add_option("-u","--username",type="string",dest="username",help="The account to upload to")
parser.add_option("-p","--password",type="string",dest="password",help="The account's password")
parser.add_option("-z","--devkey",type="string",dest="devkey",help="The Developer Key")
parser.add_option("-t","--title",type="string",dest="title",help="The movie's 'as displayed' title")
parser.add_option("-c","--category",type="string",dest="category",help="The short form category")
parser.add_option("-d","--description",type="string",dest="description",help="A text description (in quotes) for the movie")
parser.add_option("-k","--keyword",action="append",type="string",dest="keywords",help="Add a keyword, feel free to call multiple times")
parser.add_option("--list-requirements",action="store_true",dest="list_requirements",help="List required parameters")
parser.add_option("--list-options",action="store_true",dest="list_options",help="List optional parameters")
parser.add_option("--list-categories",type="string",dest="list_categories_locale",help="List available categories (pass locale - i.e. en-US)")
(options,args)=parser.parse_args()

#static querying for parameter requirements
if options.list_requirements:
    print "upztcdk" #in YouTube's case, all of them
    exit()
if options.list_options:
    exit()
    
#list categories or upload
try:
    if options.list_categories_locale:
        list_categories(options.list_categories_locale)
    else:
        auth=auth_token(options.username,options.password)
        upload_reply=upload(options.devkey,options.username,auth,args[0],
                            options.title,options.description,options.category,options.keywords)
except urllib2.URLError,e:
    print "Error (0):",e
    exit()
except urllib2.HTTPError,e:
    print "Error (1):",e
    exit()
except AuthenticationException:
    print "Error (2): Authentication failed"
    exit()
except KeywordException:
    print "Error (3): Keywords have a maximum length of 25 characters each, a total of 120 characters, and no spaces are allowed"
    exit()
    
#return the video id
try:
    reply_object=xml.dom.minidom.parse(upload_reply)
    id_element=reply_object.getElementsByTagName('id')[0].firstChild.nodeValue
    video_id=id_element[id_element.rfind('/')+1:]
except:
    print "Error (4): We appear to have uploaded but could not find the video ID in the reply back from YouTube"
    exit()
print "URL: http://www.youtube.com/watch?v=%s" % video_id

