#! /usr/bin/env bash

defaults write -g NSScrollViewRubberbanding -int 0
defaults -currentHost write com.apple.ImageCapture disableHotPlug -bool YES

# plutil -convert xml1 /System/Library/LaunchDaemons/com.apple.locationd.plist
# vi /System/Library/LaunchDaemons/com.apple.locationd.plist
# launchctl unload /System/Library/LaunchDaemons/com.apple.locationd.plist
# launchctl load /System/Library/LaunchDaemons/com.apple.locationd.plist
# pgrep -fl locationd

# plutil -convert xml1 /private/var/db/locationd/clients.plist && cat /private/var/db/locationd/clients.plist && plutil -convert binary1 /private/var/db/locationd/clients.plist
# sudo plutil -convert xml1 /private/var/db/locationd/clients.plist && sudo vi /private/var/db/locationd/clients.plist
# cat /private/var/db/locationd/clients.plist
# plutil -convert binary1 /private/var/db/locationd/clients.plist

# Installing git
# NO_GETTEXT=1 make all CFLAGS="-I/usr/local/openssl-1.0.2h/include" LDFLAGS="-L/usr/local/openssl-1.0.2h/lib"

